<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");


use CIBlock;
use CIBlockElement;
CModule::IncludeModule("iblock");

$servicesId = [];
if (!empty($_GET['id'])) {
    $servicesId = explode(',', urldecode($_GET['id']));
}

try {
    $request = CIBlockElement::GetList(
        Array("SORT" => "ASC"),
        Array("IBLOCK_ID" => 1, 'ID' => $servicesId, 'ACTIVE' => "Y"),
        false,
        false,
        Array("ID", "NAME", "PROPERTY_UNIT", "PROPERTY_NORM", "PROPERTY_PRICE", "PROPERTY_MATERIALS")
    );
    $response = [];
    while ($arrRes = $request->GetNext()) {

        $response[] = [
            "id" => $arrRes["ID"],
            "name" => $arrRes["NAME"],
            "unit" => unitSwitchHandler($arrRes["PROPERTY_UNIT_VALUE"]),
            "norm" => $arrRes["PROPERTY_NORM_VALUE"],
            "price" => $arrRes["PROPERTY_PRICE_VALUE"],
            "materials" => $arrRes["PROPERTY_MATERIALS_VALUE"],
        ];
    }

    echo json_encode($response, JSON_UNESCAPED_UNICODE);
} catch (Exception $e) {
    echo 'Ошибка: ', $e->getMessage(), "\n";
}
