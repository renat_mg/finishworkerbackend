<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");


use CIBlock;
use CIBlockElement;

CModule::IncludeModule("iblock");

$workersId = [];
if (!empty($_GET['id'])) {
    $workersId = explode(',', urldecode($_GET['id']));
}

try {

    $request = CIBlockElement::GetList(
        Array("SORT" => "ASC"),
        Array("IBLOCK_ID" => 3, 'ID' => $workersId, 'ACTIVE' => "Y"),
        false,
        false,
        Array(
            'IBLOCK_ID',
            'ID',
            'NAME',
            'PREVIEW_TEXT',
            'PREVIEW_PICTURE',
            'DETAIL_TEXT',
            'PROPERTY_POSITION',
            'PROPERTY_SERVICES',
            'PROPERTY_DATE_EXP',
            'PROPERTY_DATE_BIRDTH'
        )
    );
    $response = [];

    while ($arrRes = $request->GetNext()) {
        // $objDateTime = new DateTime($arrRes["PROPERTY_DATE_BIRDTH_VALUE"]);

        $response[] = [
            "id" => $arrRes["ID"],
            "name" => $arrRes["NAME"],
            "position" => $arrRes["PROPERTY_POSITION_VALUE"],
            "services" => $arrRes["PROPERTY_SERVICES_VALUE"],
            "birthDate" => (new DateTime($arrRes["PROPERTY_DATE_BIRDTH_VALUE"]))->format("Y-m-d"),
            "experience" => (new DateTime($arrRes["PROPERTY_DATE_EXP_VALUE"]))->format("Y-m-d"),
            // только для теста, убрать http...
            "photo" => 'http://fw.dmuhin.beget.tech'.CFile::ResizeImageGet($arrRes["PREVIEW_PICTURE"], ["width" => 300, "height" => 300], BX_RESIZE_IMAGE_PROPORTIONAL, false)['src'],
            "expDesc" => $arrRes["PREVIEW_TEXT"],
            "addInfo" => $arrRes["DETAIL_TEXT"]
        ];
    }

    echo json_encode($response, JSON_UNESCAPED_UNICODE);
} catch (Exception $e) {
    echo 'Ошибка: ', $e->getMessage(), "\n";
}
