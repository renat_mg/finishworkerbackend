<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");


use CIBlock;
use CIBlockElement;
CModule::IncludeModule("iblock");

$matsId = [];
if (!empty($_GET['id'])) {
    $matsId = explode(',', urldecode($_GET['id']));
}

try {

    $request = CIBlockElement::GetList(
        Array("SORT" => "ASC"),
        Array("IBLOCK_ID" => 2, 'ID' => $matsId, 'ACTIVE' => "Y"),
        false,
        false,
        Array(
            'IBLOCK_ID',
            'ID',
            'NAME',
            'PROPERTY_UNIT',
            'PROPERTY_PRICE',
            'PROPERTY_NORM',
            'PROPERTY_FULL_NAME',
            'PROPERTY_WEIGHT',
            'PROPERTY_VOLUME',
            'PROPERTY_PICTURES',
            'PREVIEW_TEXT'
        )
    );
    $response = [];

    while ($arrRes = $request->GetNext()) {
        // $objDateTime = new DateTime($arrRes["PROPERTY_DATE_BIRDTH_VALUE"]);

        $pictures = [];
        foreach ($arrRes['PROPERTY_PICTURES_VALUE'] as $pic) {
            $pictures[] = 'http://fw.dmuhin.beget.tech' . CFile::ResizeImageGet($pic, ["width" => 300, "height" => 300], BX_RESIZE_IMAGE_PROPORTIONAL, false)['src'];
        }
//$response[] = $arrRes;

        $response[] = [
            "id" => $arrRes["ID"],
            "name" => $arrRes["NAME"],
            "unit" => unitSwitchHandler($arrRes["PROPERTY_UNIT_VALUE"]),
            "price" => $arrRes["PROPERTY_PRICE_VALUE"],
            "norm" => $arrRes["PROPERTY_NORM_VALUE"],
            "weight" => $arrRes["PROPERTY_WEIGHT_VALUE"],
            "volume" => $arrRes["PROPERTY_VOLUME_VALUE"],
            "fullName" => $arrRes["PROPERTY_FULL_NAME_VALUE"],
            "description" => $arrRes["PREVIEW_TEXT"],
            "pictures" => $pictures
        ];
    }

    echo json_encode($response, JSON_UNESCAPED_UNICODE);
} catch (Exception $e) {
    echo 'Ошибка: ', $e->getMessage(), "\n";
}
